import Cocoa

// 1. Move type of "Did Change Closure" to own type alias
// 2. Allow "Did Change Closure" to be set through `init`

@propertyWrapper
struct RobertsWrapper {

	typealias Value = String

	typealias DidChangeValueBlock = (_ newValue: Value) -> Void

	private var storedValue: Value

	var didChangeValueBlock: DidChangeValueBlock?

	var wrappedValue: Value {
		get {
			return formattedValue
		}
		set {
			storedValue = newValue
			didChangeValueBlock?(newValue)
		}
	}

	public init(wrappedValue value: Value) {
		self.storedValue = value
	}

	var formattedValue: Value {
		return "Greeting: \(storedValue)"
	}

}

struct RobertsDecoratedStruct {

	@RobertsWrapper var myValue: String

	init(_ myValue: String) {
		self.myValue = myValue
		_myValue.didChangeValueBlock = { newValue in print(newValue) }
	}

}

var rds = RobertsDecoratedStruct("Bea")

rds.myValue = "Tim"
rds.myValue

rds.myValue = "Klaus"
rds.myValue

rds.myValue = "Olaf"
rds.myValue
