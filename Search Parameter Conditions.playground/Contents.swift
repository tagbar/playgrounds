import Cocoa

let nameFromApi: Optional<String> = "Ralph"
// let c = nameFromApi as Any as? String

let nameDescription: String = {
	guard let name = nameFromApi else {
		return "Name Unknown"
	}

	return "Name \(name)"
}()

let nameDescription2: String = {
	if let name = nameFromApi {
		return "Name \(name)"
	}

	return "Name Unknown"
}()

/// Input: `Bool` for `shouldDisplaySearch`
/// Input: `SearchConfiguration` a very complex but optional tuple
/// Input: `SearchTerm` an optional string that may be empty

typealias SearchConfiguration = (id: UUID?, searchTermDisplayThreshold: Int)
typealias SearchTerm = String

let shouldDisplaySearch = false
let searchConfiguration: SearchConfiguration? = nil
let searchTerm: SearchTerm? = ""
let searchNumberOfItems = 7

let searchIsVisibleA: Bool = {
	if !shouldDisplaySearch || searchConfiguration == nil {
		return false
	}

	if searchTerm != nil, searchTerm?.isEmpty != false, shouldDisplaySearch {
		return true
	}

	if searchNumberOfItems >= searchConfiguration?.searchTermDisplayThreshold ?? Int.max {
		return true
	}

	return false
}()

// let hasActiveSearch, …

let searchIsVisibleB: Bool = {
	guard let searchConfiguration = searchConfiguration else {
		return false
	}

	guard shouldDisplaySearch else {
		return false
	}

	guard searchTerm?.isEmpty == false else {
		return false
	}

	guard searchNumberOfItems > searchConfiguration.searchTermDisplayThreshold else {
		return false
	}

	return true
}()

let searchIsVisibleC: Bool = {
	guard
		let searchConfiguration = searchConfiguration,
		let searchTerm = searchTerm,
		!searchTerm.isEmpty,
		shouldDisplaySearch,
		searchNumberOfItems > searchConfiguration.searchTermDisplayThreshold
	else {
		return false
	}

	return true
}()

















