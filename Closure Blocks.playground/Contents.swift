import Cocoa

struct Value: CustomStringConvertible {

	var description: String
	var debugDescription: String

	typealias Values = (description: String, debugDescription: String)
	typealias ValueBlock = () -> Values

	// var valueBlock: () -> (description: String, debugDescription: String)

	init(_ block: ValueBlock) {
		let (description, debugDescription) = block()
		self.description = description
		self.debugDescription = debugDescription
	}

}

let value = Value {
	("Hello", "Hello (but for debugging)")
}

value.debugDescription


